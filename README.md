## SkillBox DevOps. Docker. Часть 6. Как работает Docker. Взгляд изнутри

SkillBox DevOps. Docker. Часть 6. Как работает Docker. Взгляд изнутри

--------------------------------------------------------

1. Делаю скриншот вывода docker info с настройками по умолчанию:

![Screenshot0](png/docker_info-0.png?raw=true "Screenshot #0") 

2. Создаю конфигурационный файл для Dockerd /etc/docker/daemon.json

https://gitlab.com/sb721407/docker/d6/-/blob/main/daemon.json

3. Останавливаю и вновь запускаю докер:

```
systemctl stop docker
systemctl start docker
```

4. Делаю скриншот вывода docker info с измененными настройками:

![Screenshot1](png/docker_info-1.png?raw=true "Screenshot #1")

5. Удаляю /etc/docker/daemon.json

6. Сохраняю копию юнит-файла systemd, запускающего docker:

```
cp /usr/lib/systemd/system/docker.service /usr/lib/systemd/system/docker.service.old
```

Его копия:

https://gitlab.com/sb721407/docker/d6/-/blob/main/docker.service.old

7. Редактирую файл /usr/lib/systemd/system/docker.service (комментирую старую строку ExecStart и вставляю новую с указанием нового Storage Driver vfs):

https://gitlab.com/sb721407/docker/d6/-/blob/main/docker.service

8. Обновляю systemd и запускаю docker с учетом изменений в юнит-файле:

```
systemctl daemon-reload
systemctl stop docker
systemctl start docker
```

9. Делаю скриншот вывода docker info с новыми настройками:

![Screenshot2](png/docker_info-2.png?raw=true "Screenshot #2")

10. Делаю скриншот содержимого новой директории /var/lib/docker/vfs:

![Screenshot3](png/storage_driver_vfs.png?raw=true "Screenshot #3")
 
